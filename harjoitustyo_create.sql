/*
Tiedonhallinta ja SQL
Opintojakson harjoitustyö - Tietokonepelin kommuunisivusto
Juha Oinonen 2022
*/

# ---------------------------------------------------------------------- #
# Luodaan ja otetaan tietokanta käyttöön.                                #
# ---------------------------------------------------------------------- #

DROP DATABASE IF EXISTS Pelisivusto;

CREATE DATABASE IF NOT EXISTS Pelisivusto;

USE Pelisivusto;

# ---------------------------------------------------------------------- #
# Varmistetaan taulujen pudotus.                                         #
# ---------------------------------------------------------------------- #

DROP TABLE IF EXISTS Kayttajat, Artikkelit, Pelit, Joukkueet, Tiedostot, Kayttajien_artikkelit, Kayttajien_joukkueet, Kayttajien_tiedostot, Turnaukset, Turnauksien_joukkueet, Kommentit, Kayttajien_kommentit, Artikkelien_kommentit, Tiedostojen_kommentit;

# ---------------------------------------------------------------------- #
# Luodaan taulut.                                                        #
# ---------------------------------------------------------------------- #

# ---------------------------------------------------------------------- #
# Käyttäjät                                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE Kayttajat (
  KayttajaID INT NOT NULL AUTO_INCREMENT,
  Etunimi VARCHAR(32),
  Sukunimi VARCHAR(32),
  Nimimerkki VARCHAR(16) NOT NULL,
  Email VARCHAR(32) NOT NULL,
  SteamID VARCHAR(16) NOT NULL,
  Kotimaa VARCHAR(32),  
  Syntymapvm DATE,
  Luontiaika TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT `Kayttajan_PK` PRIMARY KEY (KayttajaID),
  CONSTRAINT `Kayttajan_Nimimerkki_UQ` UNIQUE (Nimimerkki),
  CONSTRAINT `Kayttajan_Email_UQ` UNIQUE (Email),
  CONSTRAINT `Kayttajan_SteamID_UQ` UNIQUE (SteamID)
);

-- Indeksit
CREATE INDEX `Etunimi` ON `Kayttajat` (`Etunimi`);
CREATE INDEX `Nimimerkki` ON `Kayttajat` (`Nimimerkki`);
CREATE INDEX `Email` ON `Kayttajat` (`Email`);
CREATE INDEX `SteamID` ON `Kayttajat` (`SteamID`);

-- Täytetään taulua
INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Lio', 'Nyor', 'lionyor', 'lionyor@gmail.com', 'STEAM_0:1:00', 'Mongolia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Trus', 'Lan', 'Truslan', 'Truslan@gmail.com', 'STEAM_0:1:01', 'Liberia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Cer', 'Elm', 'cerelm', 'cerelm@gmail.com', 'STEAM_0:1:02', 'Niger', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Star', 'Dust', 'stardust', 'stardust@gmail.com', 'STEAM_0:1:03', 'Norway', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Ver', 'Dant', 'verdant', 'verdant@gmail.com', 'STEAM_0:1:04', 'Switzerland', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Pa', 'Ncakes', 'pancakes', 'pancakes@gmail.com', 'STEAM_0:1:05', 'Senegal', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Th', 'Ething', 'TheThing', 'thething@gmail.com', 'STEAM_0:1:06', 'French Polynesia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Snake', 'Rat', 'snakerat', 'snakerat@gmail.com', 'STEAM_0:1:07', 'Saint Lucia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Abe', 'Ell', 'abell1835', 'abell1835@gmail.com', 'STEAM_0:1:08', 'Libya', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Net', 'Eble', 'netbele', 'neteble@gmail.com', 'STEAM_0:1:09', 'North Macedonia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Ablo', 'Sun', 'abloomsun', 'abloomsun@gmail.com', 'STEAM_0:1:10', 'Czech Republic', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Peter', 'Olo', 'piccolo', 'piccolo@gmail.com', 'STEAM_0:1:11', 'Belarus', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Opp', 'Lent', 'opulent', 'opulent@gmail.com', 'STEAM_0:1:12', 'Micronesia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Richard', 'Dark', 'rooster', 'rooster@gmail.com', 'STEAM_0:1:13', 'American Samoa', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000)), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Dewey', 'Anderson', 'downfall', 'downfall@gmail.com', 'STEAM_0:1:14', 'Lebanon', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Ave', 'Maria', 'avalanche', 'avalanche@gmail.com', 'STEAM_0:1:15', 'Japan', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Ted', 'Mosby', 'toadsalt', 'toadsalt@gmail.com', 'STEAM_0:1:16', 'Abkhazia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Rosa', 'Park', 'rosefig', 'rosefig@gmail.com', 'STEAM_0:1:17', 'Syria', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Verner', 'Troy', 'voyager1', 'voyager1@gmail.com', 'STEAM_0:1:18', 'Cambodia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Kyle', 'Melkor', 'kaletenor', 'kaletenor@gmail.com', 'STEAM_0:1:19', 'Senegal', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Isaac', 'Newton', 'icerose', 'icerose@gmail.com', 'STEAM_0:1:20', 'Brazil', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Sal', 'Corleone', 'salacia', 'salacia@gmail.com', 'STEAM_0:1:21', 'East Timor', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Andy', 'Yd', 'andomeda', 'andomeda@gmail.com', 'STEAM_0:1:22', 'Somalia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Thomas', 'Anderson', 'turtleant', 'turtleant@gmail.com', 'STEAM_0:1:23', 'New Zealand', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Ned', 'Gary', 'ngc1569', 'ngc1569@gmail.com', 'STEAM_0:1:24', 'French Guiana', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Warry', 'Richardson', 'whirpool', 'whirpool@gmail.com', 'STEAM_0:1:25', 'Iran', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Cele', 'Nix', 'celerynix', 'celerynix@gmail.com', 'STEAM_0:1:26', 'Turks and Caicos Islands', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Max', 'Snow', 'snowstorm', 'snowstorm@gmail.com', 'STEAM_0:1:27', 'Guam', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Melanie', 'Red', 'melodyig', 'melodyig@gmail.com', 'STEAM_0:1:28', 'Hungary', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Gal', 'Fisherson', 'goldfish', 'goldfish@gmail.com', 'STEAM_0:1:29', 'Niger', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Sten', 'Sloan', 'stormcake', 'stormcake@gmail.com', 'STEAM_0:1:30', 'Saudi Arabia', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Wol', 'Rine', 'wolverine', 'wolverine@gmail.com', 'STEAM_0:1:31', 'Thailand', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Sk', 'Got', 'skatgoat', 'skatgoat@gmail.com', 'STEAM_0:1:32', 'Guinea-Bissau', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Dean', 'Watson', 'se7endrum', 'se7endrum@gmail.com', 'STEAM_0:1:33', 'Papua new Guinea', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Lock', 'One', 'stonelock', 'stonelock@gmail.com', 'STEAM_0:1:34', 'Antigua and Barbuda', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Larry', 'Tuner', 'libratune', 'libratune@gmail.com', 'STEAM_0:1:35', 'Vanuatu', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Blaise', 'Foxington', 'blazarfox', 'blazarfox@gmail.com', 'STEAM_0:1:36', 'Zimbabwe', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Sun', 'Birb', 'sunbird', 'sunbird@gmail.com', 'STEAM_0:1:37', 'Western Sahara', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Q', 'Kut', 'kumquat', 'kumquat@gmail.com', 'STEAM_0:1:38', 'Montenegro', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

INSERT INTO kayttajat(Etunimi, Sukunimi, Nimimerkki, Email, SteamID, Kotimaa, Luontiaika, SyntymaPvm) VALUES (
'Oitis', 'Easy', 'fogcarbn', 'fogcarbn@gmail.com', 'STEAM_0:1:39', 'Vietnam', 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 
(SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - 567648000 - FLOOR(RAND()*315360000), '%Y-%m-%d') AS 'date_formatted')
);

# ---------------------------------------------------------------------- #
# Artikkelit                                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE Artikkelit (
  ArtikkeliID INT NOT NULL AUTO_INCREMENT,
  Artikkeli TEXT NOT NULL,
  Aikaleima TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Otsikko VARCHAR(128) NOT NULL,
  CONSTRAINT `Artikkelin_PK` PRIMARY KEY (ArtikkeliID),
  CONSTRAINT `Artikkelin_Otsikko_UQ` UNIQUE (Otsikko)
);

-- Täytetään taulua
INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Consectetur adipiscing elit. Donec sapien ipsum, tempor non arcu id, faucibus vulputate nulla. Ut sit amet vehicula nibh. Phasellus egestas sodales magna in aliquet. Pellentesque ullamcorper ut arcu et dignissim. Vivamus at nisi a orci vehicula dapibus. Quisque id diam ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam erat volutpat. Phasellus consectetur, nisi eget commodo dictum, est libero dictum nisl, a laoreet dui neque in lectus.',
now(),
'Lorem ipsum dolor sit amet!');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Etiam elementum nisi sapien, ut imperdiet arcu laoreet non. Sed porta metus nec ante porttitor eleifend. Suspendisse potenti. Ut vel imperdiet libero. Curabitur commodo dui lectus, a finibus metus lacinia non. Maecenas tempus enim purus, vitae egestas risus efficitur a. Donec nec ornare nunc. Sed imperdiet ac magna et scelerisque. Etiam nec odio ornare, condimentum arcu et, ornare lectus. Suspendisse egestas turpis mollis neque varius auctor. Fusce iaculis condimentum blandit. Etiam id massa tortor. Quisque porta massa sed dui molestie maximus.',
'2020-09-09 02:12:57',
'In orci ligula, consectetur eget maximus eu, imperdiet pulvinar lectus');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Sed sit amet libero lacus. Quisque aliquet diam sapien, non luctus urna efficitur quis. In quis feugiat lacus. Quisque porta augue nec justo aliquam, quis varius odio semper. Praesent volutpat pretium lacus nec commodo. Sed malesuada, mauris eget mattis sollicitudin, massa nulla posuere turpis, vitae consectetur felis leo et felis. In nec arcu vulputate, eleifend ante eget, luctus nisl.',
'2021-04-29 13:44:05',
'Ut accumsan et nisi a pellentesque');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Pellentesque aliquam orci ultricies, placerat enim eget, ultricies nibh. Pellentesque pulvinar rhoncus nisi at consequat. Vestibulum non magna non libero gravida pulvinar. Cras ut odio rutrum, sollicitudin lacus at, vestibulum nisi. Fusce orci ex, efficitur sit amet nisi at, vestibulum tempor ligula. Morbi tellus magna, egestas eget ultrices ac, viverra et arcu. Fusce ut varius justo, quis pellentesque elit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla molestie in ligula faucibus tempus. Cras non imperdiet velit. In quis placerat eros. Sed dignissim augue suscipit tortor feugiat, convallis venenatis velit vestibulum. Nulla convallis massa sed ligula sagittis dictum. ',
'2017-01-27 01:05:03',
'Etiam nec quam vitae ex auctor dictum');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Nunc at odio sagittis, lacinia eros at, ultricies arcu. Aenean ut elementum augue. Donec rutrum eros risus, eu faucibus libero iaculis id. Nunc pharetra quam purus, vel dapibus velit vulputate in. Vivamus eu tortor finibus, iaculis erat at, placerat quam. Morbi a purus enim. Ut vel purus bibendum, eleifend ex sed, rhoncus urna. Proin a odio consequat, sollicitudin leo dapibus, tincidunt dolor. Pellentesque dapibus, purus at placerat aliquam, dolor magna varius magna, vitae consequat lorem sem a mauris. Ut ac semper felis, gravida faucibus nibh.',
'2005-06-27 13:37:00',
'Sed nec cursus dolor');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Fringilla massa dictum, eleifend magna. Vivamus eget justo imperdiet, vehicula neque quis, placerat lectus. Etiam eu faucibus ligula. Aliquam a facilisis dolor. Proin scelerisque vitae erat ut semper. Vivamus mollis mattis ex sit amet aliquam. Morbi vehicula nisl eu magna tristique, vitae imperdiet velit eleifend. Maecenas eu velit odio. Duis feugiat iaculis lorem ac cursus. Nunc pretium convallis ex sed convallis. Aliquam maximus eget ligula ac ultrices. ',
'2012-11-18 10:24:36',
'Morbi a quam bibendum');

INSERT INTO artikkelit (Artikkeli, Aikaleima, Otsikko) VALUES (
'Turpis ac vulputate vehicula, nunc dui sagittis justo, quis vestibulum magna sem sed nisl. In nec neque ut sapien euismod egestas eget vel massa. Nunc pretium nibh metus, sit amet consectetur quam rutrum sit amet. Cras dictum in lorem non rhoncus. Maecenas lobortis dapibus rutrum. Proin elementum ligula vel ligula imperdiet molestie. Aenean et velit in lectus euismod egestas. Ut commodo libero in egestas lacinia. Quisque eleifend ipsum libero, quis varius nulla tristique sit amet. Ut varius luctus erat, eu porta augue vehicula non. In vitae justo vitae dui porttitor vulputate. Cras et molestie erat. In suscipit ligula mi, non pretium orci posuere non. Nulla vitae nibh non urna interdum dignissim eget blandit leo. Sed accumsan pulvinar purus, vel fermentum nibh malesuada non. ',
'2006-06-28 17:01:23',
'Nulla bibendum');

# ---------------------------------------------------------------------- #
# Peli                                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE Pelit (
  PeliID INT NULL AUTO_INCREMENT,
  Nimi VARCHAR(64) NOT NULL,
  Kuvaus TEXT,
  JoukkueenKoko INT NOT NULL,
  CONSTRAINT `Pelin_PK` PRIMARY KEY (PeliID),
  CONSTRAINT `Pelin_Nimi_UQ` UNIQUE (Nimi)
);

-- Täytetään taulua
INSERT INTO Pelit (Nimi, Kuvaus, JoukkueenKoko) VALUES ('Apex Legends', 'Arcade FPS shooter', 3);
INSERT INTO Pelit (Nimi, Kuvaus, JoukkueenKoko) VALUES ('DOTA 2', 'ARPG Brawler', 5);
INSERT INTO Pelit (Nimi, Kuvaus, JoukkueenKoko) VALUES ('Natural Selection', 'Tactical & Strategical FPS shooter with melee combat', 6);

# ---------------------------------------------------------------------- #
# Joukkueet                                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE Joukkueet (
  JoukkueID INT NOT NULL AUTO_INCREMENT,
  Nimi VARCHAR(24) NOT NULL,
  Kotisivu VARCHAR(32),
  Kotimaa VARCHAR(32),
  CONSTRAINT `Joukkueen_PK` PRIMARY KEY (JoukkueID),
  CONSTRAINT `Joukkueen_Nimi_UQ` UNIQUE (Nimi)
);

-- Indeksit
CREATE INDEX `Nimi` ON `Joukkueet` (`Nimi`);
CREATE INDEX `Kotimaa` ON `Joukkueet` (`Kotimaa`);

-- Täytetään taulua
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('All-In', 'http://maecenas.ca', 'Canada');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Legendary Snails', 'http://sit.fr', 'France');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('SnowRollers', 'http://amet.se', 'Sweden');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('FRP', 'http://sodales.fi', 'Finland');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('priori', 'http://ultricies.com', 'USA');

INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Like a Glove', 'http://malesuada.co.au', 'Australia');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Singularity', 'http://vulputate.com', 'USA');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Knife', 'http://knife-gaming.com', 'Europe');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('vetus', 'http://vetus.fi', 'Finland');

INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Lerk Force One', 'http://exsed.fr', 'France');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('MedPack!', 'http://pellentesque.es', 'Spain');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Destination Unknown', 'http://faucibus.com', 'Finland');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('YO Clan', 'http://quisque.com', 'Europe');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Nine Legends', 'http://duifaucibus.com', 'Europe');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('Quaxy', 'http://vestibulum.com', 'Finland');
INSERT INTO Joukkueet (Nimi, Kotisivu, Kotimaa) VALUES ('RageQuit', 'http://efficitur.de', 'Germany');

# ---------------------------------------------------------------------- #
# Tiedostot                                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE Tiedostot (
  TiedostoID INT NOT NULL AUTO_INCREMENT,
  Nimi VARCHAR(128) NOT NULL,  
  Aikaleima TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Sisalto LONGBLOB,
  CONSTRAINT `Tiedoston_PK` PRIMARY KEY (TiedostoID),
  CONSTRAINT `Tiedoston_Nimi_UQ` UNIQUE (Nimi)
);

-- Indeksit
CREATE INDEX `Nimi` ON `Tiedostot` (`Nimi`);

-- Täytetään taulua
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('NS Mappack', NOW(), 'Sisaltaa karttoja, tama on vain random teksti');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('NS Demopack', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'NS demoja, Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Gamma Panel', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Gamma Panel. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Vampire Slayer Installer', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Vampire Slayer Installer. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('TeamSpeak 3', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'TeamSpeak 3. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Radial', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'NS1 Frag Movie. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Steam', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Steam. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Pro player configs', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Configs. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Apex Guide To Pro Gaming', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Guide. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Practice routine', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'Practice routine. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');
INSERT INTO tiedostot(Nimi, Aikaleima, Sisalto) VALUES ('Slightly suspicious rar package', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 'openthis.rar. Tama on vain random tekstia, jotta ei tarvitse lisata oikeita tiedostoja.');

# ---------------------------------------------------------------------- #
# Turnaukset                                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE Turnaukset (
  TurnausID INT NOT NULL AUTO_INCREMENT,
  Nimi VARCHAR(64) NOT NULL,
  Kuvaus TEXT NOT NULL,
  Aloituspvm DATE NOT NULL,
  Lopetuspvm DATE NOT NULL,
  Saannot TEXT NOT NULL,
  PeliID INT NOT NULL,
  CONSTRAINT `Turnauksen_PK` PRIMARY KEY (TurnausID),
  CONSTRAINT `Turnauksen_Nimi_UQ` UNIQUE (Nimi)
);

-- Indeksit
CREATE INDEX `Nimi` ON `Turnaukset` (`Nimi`);

-- Täytetään taulua
INSERT INTO Turnaukset (Nimi, Kuvaus, Aloituspvm, Lopetuspvm, Saannot, PeliID) VALUES ('Apex Legends LAUNCH EVENT!', 'Tournament for Apex Legends launch', '2019-02-04', '2019-03-01', 'http://rules.com', 1);
INSERT INTO Turnaukset (Nimi, Kuvaus, Aloituspvm, Lopetuspvm, Saannot, PeliID) VALUES ('Apex Legends S02', 'Tournament for Apex Legends Season 02', '2020-05-01', '2020-06-01', 'http://rules.com', 1);

INSERT INTO Turnaukset (Nimi, Kuvaus, Aloituspvm, Lopetuspvm, Saannot, PeliID) VALUES ('Dota 2 Suomi #03', 'Dota 2 SUOMALAINEN turnaus!', '2022-07-20', '2022-08-23', 'Pitäkää hauskaa!', 2);
INSERT INTO Turnaukset (Nimi, Kuvaus, Aloituspvm, Lopetuspvm, Saannot, PeliID) VALUES ('The Intergalactic 2', 'Most prestigious tournament for Dota, aliens & humans welcome!', '2030-12-22', '2031-02-24', '8273450712034712', 2);

INSERT INTO Turnaukset (Nimi, Kuvaus, Aloituspvm, Lopetuspvm, Saannot, PeliID) VALUES ('Natural Selection Night Cup #1', 'Weekend cup for fun', '2019-04-01', '2019-04-02', 'Same as usual.', 3);

# ---------------------------------------------------------------------- #
# Kommentit                                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE Kommentit (
  KommenttiID INT NOT NULL AUTO_INCREMENT,
  Kommentti VARCHAR(256) NOT NULL,
  Aikaleima TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Kommentin_kommentit_KommenttiID INT,
  KayttajaID INT NOT NULL,
  CONSTRAINT `Kommentin_PK` PRIMARY KEY (KommenttiID)
);

-- Täytetään taulua
-- Artikkelien kommentit
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Vestibulum non lectus eget orci. ', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 35);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nam tristique massa a odio porttitor sagittis.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 14);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Proin rutrum ipsum non venenatis facilisis.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 3);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Integer imperdiet mauris condimentum velit elementum condimentum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 5);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nullam eget erat et mauris feugiat fringilla.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 14);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Aenean consectetur eros vulputate, consectetur ipsum et, tincidunt tellus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 7);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Proin mattis ligula in mi elementum fermentum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 24);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Pellentesque dictum neque sed tortor sagittis euismod.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 25);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nam eget nunc ac enim rhoncus viverra a a risus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 22);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Aenean congue erat sit amet massa gravida rutrum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 38);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Phasellus efficitur ante et velit facilisis viverra sit amet vel arcu.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 1);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Vestibulum id dui sit amet odio dapibus viverra.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 36);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nunc placerat massa ut diam facilisis, ac maximus est fermentum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 5);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Etiam dictum tellus non nibh faucibus, sit amet maximus dolor rutrum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 9);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Proin nec nibh malesuada, consectetur libero nec, commodo massa.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 34);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nulla vitae lectus vel ex venenatis aliquam id non est.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 18);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Morbi vitae libero at ante gravida blandit non in urna.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 21);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Mauris aliquet nunc commodo enim euismod, non interdum nibh ornare.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 11);

INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Fusce pharetra mi ut arcu consequat pulvinar.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 17);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Quisque porttitor ipsum non arcu vehicula, sed pretium est tempus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 30);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Sed vel ipsum quis nisi maximus commodo.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 4);

-- Kayttajien kommentit
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Mauris a urna ultricies, viverra magna vel, venenatis mi.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 23);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Integer vel turpis nec nibh condimentum tincidunt et faucibus nisi.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 4);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Donec finibus tortor venenatis dui tempus, quis accumsan augue mollis.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 5);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Suspendisse euismod erat at leo lobortis, at pellentesque purus sodales.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 16);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Sed sed sem euismod ligula pretium dapibus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 32);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Fusce vehicula lectus ut leo tristique aliquet.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 26);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Praesent facilisis lacus vel augue aliquet, placerat condimentum metus vulputate.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 39);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Pellentesque interdum ante et lorem imperdiet dignissim.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 2);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Curabitur eu sapien id ante porttitor tempus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 13);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Aliquam in felis id orci viverra molestie.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 39);

-- Tiedostojen kommentit
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Sed a erat eu magna accumsan commodo.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 29);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Cras et risus congue, faucibus urna vel, rutrum felis.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 27);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Suspendisse dignissim nibh a luctus pharetra.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 38);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Cras tempus orci eget quam imperdiet, et tincidunt augue mattis.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 26);
INSERT INTO kommentit (Kommentti, Aikaleima, KayttajaID) VALUES ('Nullam auctor erat eu nisl semper, in scelerisque diam consequat.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 1);

-- Kommenttien kommentit
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('In semper nulla a mi varius, tincidunt sagittis elit condimentum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 4, 3);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Phasellus egestas metus vitae placerat faucibus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 37, 23);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Etiam ac lectus rutrum, suscipit eros sit amet, efficitur ipsum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 37, 19);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Sed sit amet massa at odio semper interdum non non lacus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 38, 21);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Quisque pellentesque risus quis justo aliquam fermentum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 2, 9);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Aenean imperdiet eros eu nisl vehicula, et eleifend nisi sollicitudin.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 4, 29);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Duis eget elit eget ante iaculis auctor.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 6, 5);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Quisque luctus nunc vel mauris posuere, non elementum tellus hendrerit.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 6, 4);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Phasellus vel nibh vel tortor fringilla porttitor.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 6, 24);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Pellentesque at nibh pretium, suscipit erat sed, iaculis dui.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 8, 38);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Fusce et lorem a orci mollis lacinia.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 8, 4);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Proin non tellus sed nulla consequat mattis eget in ante.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 10, 32);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Duis eget mi pulvinar, cursus dolor vel, consequat ipsum.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 12, 16);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Quisque sit amet eros vitae elit fermentum porttitor ac sit amet tortor.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 14, 37);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Phasellus sed diam sed ligula cursus tristique.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 16, 24);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Morbi a leo sed dui faucibus porttitor.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 18, 11);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Nullam eu nisi faucibus, auctor tortor pharetra, tempor quam.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 20, 34);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Donec sit amet ipsum malesuada, egestas turpis sed, ultrices urna.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 20, 8);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Mauris condimentum nisl eu sapien tincidunt, in aliquet eros volutpat.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 22, 18);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Donec a neque sit amet enim ornare gravida vel elementum lorem.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 24, 19);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Praesent vitae neque mattis, ultricies turpis non, euismod mauris.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 26, 32);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Sed tincidunt ipsum eget turpis aliquam consectetur eu at magna.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 28, 8);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Pellentesque mollis justo vitae erat rutrum cursus.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 30, 15);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Sed id lorem et purus porta gravida.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 32, 9);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Mauris ut arcu euismod, placerat lectus at, consectetur libero.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 34, 13);
INSERT INTO kommentit (Kommentti, Aikaleima, Kommentin_kommentit_KommenttiID, KayttajaID) VALUES ('Curabitur congue libero rhoncus, porta metus vitae, efficitur massa.', (SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(RAND()*315360000))), 36, 19);

# ---------------------------------------------------------------------- #
# Yhdistävät taulut                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE Artikkelien_kommentit (
  KommenttiID INT NOT NULL,
  ArtikkeliID INT NOT NULL,
  CONSTRAINT `AK_KommenttiID_UQ` UNIQUE (KommenttiID)
);

-- Täytetään taulua
INSERT INTO artikkelien_kommentit VALUES (1, 1); INSERT INTO artikkelien_kommentit VALUES (2, 1); INSERT INTO artikkelien_kommentit VALUES (3, 1);
INSERT INTO artikkelien_kommentit VALUES (4, 1); INSERT INTO artikkelien_kommentit VALUES (5, 1);

INSERT INTO artikkelien_kommentit VALUES (6, 2);

INSERT INTO artikkelien_kommentit VALUES (7, 3); INSERT INTO artikkelien_kommentit VALUES (8, 3);

INSERT INTO artikkelien_kommentit VALUES (9, 4); INSERT INTO artikkelien_kommentit VALUES (10, 4); INSERT INTO artikkelien_kommentit VALUES (11, 4);
INSERT INTO artikkelien_kommentit VALUES (12, 4); INSERT INTO artikkelien_kommentit VALUES (13, 4); INSERT INTO artikkelien_kommentit VALUES (14, 4);

INSERT INTO artikkelien_kommentit VALUES (15, 5); INSERT INTO artikkelien_kommentit VALUES (16, 5);

INSERT INTO artikkelien_kommentit VALUES (17, 6); INSERT INTO artikkelien_kommentit VALUES (18, 6);

INSERT INTO artikkelien_kommentit VALUES (19, 7); INSERT INTO artikkelien_kommentit VALUES (20, 7); INSERT INTO artikkelien_kommentit VALUES (21, 7);

CREATE TABLE Kayttajien_kommentit (
  KommenttiID INT NOT NULL,
  KayttajaID INT NOT NULL,
  CONSTRAINT `KK_KommenttiID_UQ` UNIQUE (KommenttiID)
);

-- Täytetään taulua
INSERT INTO kayttajien_kommentit VALUES(22, 21);
INSERT INTO kayttajien_kommentit VALUES(23, 18);
INSERT INTO kayttajien_kommentit VALUES(24, 36);
INSERT INTO kayttajien_kommentit VALUES(25, 9);
INSERT INTO kayttajien_kommentit VALUES(26, 22);
INSERT INTO kayttajien_kommentit VALUES(27, 16);
INSERT INTO kayttajien_kommentit VALUES(28, 2);
INSERT INTO kayttajien_kommentit VALUES(29, 19);
INSERT INTO kayttajien_kommentit VALUES(30, 17);
INSERT INTO kayttajien_kommentit VALUES(31, 32);

CREATE TABLE Tiedostojen_kommentit (
  KommenttiID INT NOT NULL,
  TiedostoID INT NOT NULL,
  CONSTRAINT `TK_KommenttiID_UQ` UNIQUE (KommenttiID)
);

-- Täytetään taulua
INSERT INTO tiedostojen_kommentit VALUES (32, 4);
INSERT INTO tiedostojen_kommentit VALUES (33, 9);
INSERT INTO tiedostojen_kommentit VALUES (34, 1);
INSERT INTO tiedostojen_kommentit VALUES (35, 9);
INSERT INTO tiedostojen_kommentit VALUES (36, 10);

CREATE TABLE Kayttajien_artikkelit (
  KayttajaID INT NOT NULL,
  ArtikkeliID INT NOT NULL,
  CONSTRAINT `KA_ArtikkeliID_UQ` UNIQUE (ArtikkeliID)
);

-- Täytetään taulua
INSERT INTO Kayttajien_artikkelit VALUES (1, 1); INSERT INTO Kayttajien_artikkelit VALUES (1, 3); INSERT INTO Kayttajien_artikkelit VALUES (1, 5);
INSERT INTO Kayttajien_artikkelit VALUES (15, 2); INSERT INTO Kayttajien_artikkelit VALUES (1, 7);
INSERT INTO Kayttajien_artikkelit VALUES (27, 6);
INSERT INTO Kayttajien_artikkelit VALUES (35, 4);

CREATE TABLE Kayttajien_tiedostot (
  TiedostoID INT NOT NULL,
  KayttajaID INT NOT NULL,
  CONSTRAINT `KT_TiedostoID_UQ` UNIQUE (TiedostoID)
);

-- Täytetään taulua
INSERT INTO kayttajien_tiedostot VALUES (1, 24); INSERT INTO kayttajien_tiedostot VALUES (2, 20); INSERT INTO kayttajien_tiedostot VALUES (3, 27);
INSERT INTO kayttajien_tiedostot VALUES (4, 26); INSERT INTO kayttajien_tiedostot VALUES (5, 40); INSERT INTO kayttajien_tiedostot VALUES (6, 3);
INSERT INTO kayttajien_tiedostot VALUES (7, 35); INSERT INTO kayttajien_tiedostot VALUES (8, 17); INSERT INTO kayttajien_tiedostot VALUES (9, 33);
INSERT INTO kayttajien_tiedostot VALUES (10, 27); INSERT INTO kayttajien_tiedostot VALUES (11, 21);

CREATE TABLE Kayttajien_joukkueet (
  KayttajaID INT NOT NULL,
  JoukkueID INT NOT NULL,
  PRIMARY KEY (KayttajaID, JoukkueID)
);

-- Täytetään taulua
INSERT INTO Kayttajien_joukkueet VALUES (1, 1); INSERT INTO Kayttajien_joukkueet VALUES (1, 2); INSERT INTO Kayttajien_joukkueet VALUES (1, 3);
INSERT INTO Kayttajien_joukkueet VALUES (2, 4); INSERT INTO Kayttajien_joukkueet VALUES (2, 5); INSERT INTO Kayttajien_joukkueet VALUES (2, 6);
INSERT INTO Kayttajien_joukkueet VALUES (3, 7); INSERT INTO Kayttajien_joukkueet VALUES (3, 8); INSERT INTO Kayttajien_joukkueet VALUES (3, 9);
INSERT INTO Kayttajien_joukkueet VALUES (4, 10); INSERT INTO Kayttajien_joukkueet VALUES (4, 11); INSERT INTO Kayttajien_joukkueet VALUES (4, 12);
INSERT INTO Kayttajien_joukkueet VALUES (5, 13); INSERT INTO Kayttajien_joukkueet VALUES (5, 14); INSERT INTO Kayttajien_joukkueet VALUES (5, 15);
INSERT INTO Kayttajien_joukkueet VALUES (6, 16); INSERT INTO Kayttajien_joukkueet VALUES (6, 1); INSERT INTO Kayttajien_joukkueet VALUES (6, 2);
INSERT INTO Kayttajien_joukkueet VALUES (7, 3); INSERT INTO Kayttajien_joukkueet VALUES (7, 4); INSERT INTO Kayttajien_joukkueet VALUES (7, 5);
INSERT INTO Kayttajien_joukkueet VALUES (8, 6); INSERT INTO Kayttajien_joukkueet VALUES (8, 7); INSERT INTO Kayttajien_joukkueet VALUES (8, 8);
INSERT INTO Kayttajien_joukkueet VALUES (9, 9); INSERT INTO Kayttajien_joukkueet VALUES (9, 10); INSERT INTO Kayttajien_joukkueet VALUES (9, 11);
INSERT INTO Kayttajien_joukkueet VALUES (10, 12); INSERT INTO Kayttajien_joukkueet VALUES (10, 13); INSERT INTO Kayttajien_joukkueet VALUES (10, 14);
INSERT INTO Kayttajien_joukkueet VALUES (11, 15); INSERT INTO Kayttajien_joukkueet VALUES (11, 16); INSERT INTO Kayttajien_joukkueet VALUES (11, 1);
INSERT INTO Kayttajien_joukkueet VALUES (12, 2); INSERT INTO Kayttajien_joukkueet VALUES (12, 3); INSERT INTO Kayttajien_joukkueet VALUES (12, 4);
INSERT INTO Kayttajien_joukkueet VALUES (13, 5); INSERT INTO Kayttajien_joukkueet VALUES (13, 6); INSERT INTO Kayttajien_joukkueet VALUES (13, 7);
INSERT INTO Kayttajien_joukkueet VALUES (14, 8); INSERT INTO Kayttajien_joukkueet VALUES (14, 9); INSERT INTO Kayttajien_joukkueet VALUES (14, 10);
INSERT INTO Kayttajien_joukkueet VALUES (15, 11); INSERT INTO Kayttajien_joukkueet VALUES (15, 12); INSERT INTO Kayttajien_joukkueet VALUES (15, 13);
INSERT INTO Kayttajien_joukkueet VALUES (16, 14); INSERT INTO Kayttajien_joukkueet VALUES (16, 15); INSERT INTO Kayttajien_joukkueet VALUES (16, 16);
INSERT INTO Kayttajien_joukkueet VALUES (17, 1); INSERT INTO Kayttajien_joukkueet VALUES (17, 2); INSERT INTO Kayttajien_joukkueet VALUES (17, 3);
INSERT INTO Kayttajien_joukkueet VALUES (18, 4); INSERT INTO Kayttajien_joukkueet VALUES (18, 5); INSERT INTO Kayttajien_joukkueet VALUES (18, 6);
INSERT INTO Kayttajien_joukkueet VALUES (19, 7); INSERT INTO Kayttajien_joukkueet VALUES (19, 8); INSERT INTO Kayttajien_joukkueet VALUES (19, 9);
INSERT INTO Kayttajien_joukkueet VALUES (20, 10); INSERT INTO Kayttajien_joukkueet VALUES (20, 11); INSERT INTO Kayttajien_joukkueet VALUES (20, 12);
INSERT INTO Kayttajien_joukkueet VALUES (21, 13); INSERT INTO Kayttajien_joukkueet VALUES (21, 14); INSERT INTO Kayttajien_joukkueet VALUES (21, 15);
INSERT INTO Kayttajien_joukkueet VALUES (22, 16); INSERT INTO Kayttajien_joukkueet VALUES (22, 1); INSERT INTO Kayttajien_joukkueet VALUES (22, 2);
INSERT INTO Kayttajien_joukkueet VALUES (23, 3); INSERT INTO Kayttajien_joukkueet VALUES (23, 4); INSERT INTO Kayttajien_joukkueet VALUES (23, 5);
INSERT INTO Kayttajien_joukkueet VALUES (24, 6); INSERT INTO Kayttajien_joukkueet VALUES (24, 7); INSERT INTO Kayttajien_joukkueet VALUES (24, 8);
INSERT INTO Kayttajien_joukkueet VALUES (25, 9); INSERT INTO Kayttajien_joukkueet VALUES (25, 10); INSERT INTO Kayttajien_joukkueet VALUES (25, 11);
INSERT INTO Kayttajien_joukkueet VALUES (26, 12); INSERT INTO Kayttajien_joukkueet VALUES (26, 13); INSERT INTO Kayttajien_joukkueet VALUES (26, 14);
INSERT INTO Kayttajien_joukkueet VALUES (27, 15); INSERT INTO Kayttajien_joukkueet VALUES (27, 16); INSERT INTO Kayttajien_joukkueet VALUES (27, 1);
INSERT INTO Kayttajien_joukkueet VALUES (28, 2); INSERT INTO Kayttajien_joukkueet VALUES (28, 3); INSERT INTO Kayttajien_joukkueet VALUES (28, 4);
INSERT INTO Kayttajien_joukkueet VALUES (29, 5); INSERT INTO Kayttajien_joukkueet VALUES (29, 6); INSERT INTO Kayttajien_joukkueet VALUES (29, 7);
INSERT INTO Kayttajien_joukkueet VALUES (30, 8); INSERT INTO Kayttajien_joukkueet VALUES (30, 9); INSERT INTO Kayttajien_joukkueet VALUES (30, 10);
INSERT INTO Kayttajien_joukkueet VALUES (31, 11); INSERT INTO Kayttajien_joukkueet VALUES (31, 12); INSERT INTO Kayttajien_joukkueet VALUES (31, 13);
INSERT INTO Kayttajien_joukkueet VALUES (32, 14); INSERT INTO Kayttajien_joukkueet VALUES (32, 15); INSERT INTO Kayttajien_joukkueet VALUES (32, 16);
INSERT INTO Kayttajien_joukkueet VALUES (33, 1); INSERT INTO Kayttajien_joukkueet VALUES (33, 2); INSERT INTO Kayttajien_joukkueet VALUES (33, 3);
INSERT INTO Kayttajien_joukkueet VALUES (34, 4); INSERT INTO Kayttajien_joukkueet VALUES (34, 5); INSERT INTO Kayttajien_joukkueet VALUES (34, 6);
INSERT INTO Kayttajien_joukkueet VALUES (35, 7); INSERT INTO Kayttajien_joukkueet VALUES (35, 8); INSERT INTO Kayttajien_joukkueet VALUES (35, 9);
INSERT INTO Kayttajien_joukkueet VALUES (36, 10); INSERT INTO Kayttajien_joukkueet VALUES (36, 11); INSERT INTO Kayttajien_joukkueet VALUES (36, 12);
INSERT INTO Kayttajien_joukkueet VALUES (37, 13); INSERT INTO Kayttajien_joukkueet VALUES (37, 14); INSERT INTO Kayttajien_joukkueet VALUES (37, 15);
INSERT INTO Kayttajien_joukkueet VALUES (38, 16); INSERT INTO Kayttajien_joukkueet VALUES (38, 11); INSERT INTO Kayttajien_joukkueet VALUES (38, 12);
INSERT INTO Kayttajien_joukkueet VALUES (39, 13); INSERT INTO Kayttajien_joukkueet VALUES (39, 14); INSERT INTO Kayttajien_joukkueet VALUES (39, 15);
INSERT INTO Kayttajien_joukkueet VALUES (40, 16); INSERT INTO Kayttajien_joukkueet VALUES (40, 1); INSERT INTO Kayttajien_joukkueet VALUES (40, 12);

CREATE TABLE Turnauksien_joukkueet (
  Sijoitus INT,
  JoukkueID INT NOT NULL,
  TurnausID INT NOT NULL,
  PRIMARY KEY (JoukkueID, TurnausID)
);

-- Täytetään taulua
-- Ensimmäinen turnaus
INSERT INTO Turnauksien_joukkueet VALUES (1, 1, 1); INSERT INTO Turnauksien_joukkueet VALUES (2, 3, 1); INSERT INTO Turnauksien_joukkueet VALUES (3, 5, 1);
INSERT INTO Turnauksien_joukkueet VALUES (4, 6, 1); INSERT INTO Turnauksien_joukkueet VALUES (5, 7, 1); INSERT INTO Turnauksien_joukkueet VALUES (6, 8, 1);
INSERT INTO Turnauksien_joukkueet VALUES (7, 11, 1); INSERT INTO Turnauksien_joukkueet VALUES (8, 13, 1); INSERT INTO Turnauksien_joukkueet VALUES (9, 15, 1);

-- Toinen turnaus ("ei päättynyt")
INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (1, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (15, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (12, 2);
INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (7, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (9, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (11, 2);
INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (3, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (14, 2); INSERT INTO Turnauksien_joukkueet (JoukkueID, TurnausID) VALUES (10, 2);

-- Kolmas turnaus
INSERT INTO Turnauksien_joukkueet VALUES (1, 2, 3); INSERT INTO Turnauksien_joukkueet VALUES (2, 4, 3); INSERT INTO Turnauksien_joukkueet VALUES (3, 6, 3);
INSERT INTO Turnauksien_joukkueet VALUES (4, 8, 3); INSERT INTO Turnauksien_joukkueet VALUES (5, 10, 3); INSERT INTO Turnauksien_joukkueet VALUES (6, 12, 3);
INSERT INTO Turnauksien_joukkueet VALUES (7, 14, 3); INSERT INTO Turnauksien_joukkueet VALUES (8, 16, 3); INSERT INTO Turnauksien_joukkueet VALUES (9, 1, 3);
INSERT INTO Turnauksien_joukkueet VALUES (10, 15, 3);

-- Neljäs turnaus
INSERT INTO Turnauksien_joukkueet VALUES (6, 16, 4); INSERT INTO Turnauksien_joukkueet VALUES (5, 15, 4); INSERT INTO Turnauksien_joukkueet VALUES (4, 14, 4);
INSERT INTO Turnauksien_joukkueet VALUES (1, 11, 4); INSERT INTO Turnauksien_joukkueet VALUES (2, 12, 4); INSERT INTO Turnauksien_joukkueet VALUES (3, 13, 4);

-- Viides turnaus
INSERT INTO Turnauksien_joukkueet VALUES (1, 1, 5); INSERT INTO Turnauksien_joukkueet VALUES (2, 2, 5); INSERT INTO Turnauksien_joukkueet VALUES (3, 3, 5);
INSERT INTO Turnauksien_joukkueet VALUES (14, 14, 5); INSERT INTO Turnauksien_joukkueet VALUES (12, 12, 5); INSERT INTO Turnauksien_joukkueet VALUES (4, 4, 5);
INSERT INTO Turnauksien_joukkueet VALUES (15, 15, 5); INSERT INTO Turnauksien_joukkueet VALUES (10, 10, 5); INSERT INTO Turnauksien_joukkueet VALUES (5, 5, 5);
INSERT INTO Turnauksien_joukkueet VALUES (13, 13, 5); INSERT INTO Turnauksien_joukkueet VALUES (9, 9, 5); INSERT INTO Turnauksien_joukkueet VALUES (6, 6, 5);
INSERT INTO Turnauksien_joukkueet VALUES (16, 16, 5); INSERT INTO Turnauksien_joukkueet VALUES (8, 8, 5); INSERT INTO Turnauksien_joukkueet VALUES (7, 7, 5);
INSERT INTO Turnauksien_joukkueet VALUES (11, 11, 5);

# ---------------------------------------------------------------------- #
# FK-rajoitukset                                                         #
# ---------------------------------------------------------------------- #

ALTER TABLE Turnaukset ADD CONSTRAINT `FK_Turnauksen_PeliID` FOREIGN KEY (PeliID) REFERENCES Pelit(PeliID);

ALTER TABLE Kommentit ADD CONSTRAINT `FK_Kommentti_KommenttiID` FOREIGN KEY (Kommentin_kommentit_KommenttiID) REFERENCES Kommentit(KommenttiID);
ALTER TABLE Kommentit ADD CONSTRAINT `FK_Kommentti_KayttajaID` FOREIGN KEY (KayttajaID) REFERENCES Kayttajat(KayttajaID);

ALTER TABLE Artikkelien_kommentit ADD CONSTRAINT `FK_AK_ArtikkeliID` FOREIGN KEY (ArtikkeliID) REFERENCES Artikkelit(ArtikkeliID);
ALTER TABLE Artikkelien_kommentit ADD CONSTRAINT `FK_AK_KommenttiID` FOREIGN KEY (KommenttiID) REFERENCES Kommentit(KommenttiID);

ALTER TABLE Tiedostojen_kommentit ADD CONSTRAINT `FK_TK_TiedostoID` FOREIGN KEY (TiedostoID) REFERENCES Tiedostot(TiedostoID);
ALTER TABLE Tiedostojen_kommentit ADD CONSTRAINT `FK_TK_KommenttiID` FOREIGN KEY (KommenttiID) REFERENCES Kommentit(KommenttiID);

ALTER TABLE Kayttajien_artikkelit ADD CONSTRAINT `FK_KA_KayttajaID` FOREIGN KEY (KayttajaID) REFERENCES Kayttajat(KayttajaID);
ALTER TABLE Kayttajien_artikkelit ADD CONSTRAINT `FK_KA_ArtikkeliID` FOREIGN KEY (ArtikkeliID) REFERENCES Artikkelit(ArtikkeliID);

ALTER TABLE Kayttajien_kommentit ADD CONSTRAINT `FK_KK_KayttajaID` FOREIGN KEY (KayttajaID) REFERENCES Kayttajat(KayttajaID);
ALTER TABLE Kayttajien_kommentit ADD CONSTRAINT `FK_KK_KommenttiID` FOREIGN KEY (KommenttiID) REFERENCES Kommentit(KommenttiID);

ALTER TABLE Kayttajien_tiedostot ADD CONSTRAINT `FK_KT_KayttajaID` FOREIGN KEY (KayttajaID) REFERENCES Kayttajat(KayttajaID);
ALTER TABLE Kayttajien_tiedostot ADD CONSTRAINT `FK_KT_TiedostoID` FOREIGN KEY (TiedostoID) REFERENCES Tiedostot(TiedostoID);

ALTER TABLE Kayttajien_joukkueet ADD CONSTRAINT `FK_KJ_KayttajaID` FOREIGN KEY (KayttajaID) REFERENCES Kayttajat(KayttajaID);
ALTER TABLE Kayttajien_joukkueet ADD CONSTRAINT `FK_KJ_JoukkueID` FOREIGN KEY (JoukkueID) REFERENCES Joukkueet(JoukkueID);

ALTER TABLE Turnauksien_joukkueet ADD CONSTRAINT `FK_TJ_TurnausID` FOREIGN KEY (TurnausID) REFERENCES Turnaukset(TurnausID);
ALTER TABLE Turnauksien_joukkueet ADD CONSTRAINT `FK_TJ_JoukkueID` FOREIGN KEY (JoukkueID) REFERENCES Joukkueet(JoukkueID);

# ---------------------------------------------------------------------- #
# Näkymien luonti                                                        #
# ---------------------------------------------------------------------- #

-- Listaa joukkueet, sekä niiden pelaajamäärät
CREATE VIEW `Joukkueiden pelaajamaarat` AS
SELECT nimi AS Joukkue, COUNT(Kayttajien_joukkueet.JoukkueID) AS `Pelaajamaara` FROM joukkueet
JOIN Kayttajien_joukkueet ON joukkueet.JoukkueID = Kayttajien_joukkueet.JoukkueID
GROUP BY Joukkue;

-- Listaa joukkueet ja niiden pelaajat
CREATE VIEW `Joukkueet ja niiden pelaajat` AS
SELECT nimi AS `Joukkue`, nimimerkki AS `Pelaaja` FROM joukkueet
JOIN kayttajien_joukkueet ON joukkueet.JoukkueID = kayttajien_joukkueet.JoukkueID
JOIN kayttajat ON kayttajien_joukkueet.KayttajaID = kayttajat.KayttajaID;

-- Listaa kaikki maat, josta on rekisteröity pelaaja ja sen maan pelaajien lukumäärän
CREATE VIEW `Maiden pelaajamaarat` AS
SELECT Kotimaa, COUNT(Kotimaa) AS `Pelaajamaara` FROM kayttajat
GROUP BY Kotimaa;

-- Listaa kaikki artikkelit ja niiden kirjoittajat
CREATE VIEW `Kayttajien artikkelit` AS
SELECT nimimerkki, otsikko FROM kayttajat
JOIN Kayttajien_artikkelit ON kayttajat.kayttajaid = Kayttajien_artikkelit.KayttajaID
JOIN artikkelit ON Kayttajien_artikkelit.artikkeliid = artikkelit.ArtikkeliID;

-- Listaa kaikki joukkueet jotka ovat osallistuneet johonkin turnaukseen, sekä turnauksen nimi
CREATE VIEW `Turnauksiin osallistuneet joukkueet` AS
SELECT joukkueet.nimi AS `Joukkue`, turnaukset.nimi AS `Turnaus` FROM turnaukset
JOIN turnauksien_joukkueet ON turnaukset.TurnausID = turnauksien_joukkueet.TurnausID
JOIN joukkueet ON turnauksien_joukkueet.JoukkueID = joukkueet.JoukkueID;

-- Turnauksiin osallistujat + peli
CREATE VIEW `Turnauksiin osallistuneet joukkueet pelin kanssa` AS
SELECT joukkueet.nimi AS `Joukkue`, turnaukset.nimi AS `Turnaus`, pelit.Nimi AS `Peli` FROM turnaukset
JOIN turnauksien_joukkueet ON turnaukset.TurnausID = turnauksien_joukkueet.TurnausID
JOIN joukkueet ON turnauksien_joukkueet.JoukkueID = joukkueet.JoukkueID
JOIN pelit ON turnaukset.PeliID = pelit.PeliID;

-- Listaa kaikki turnaukset ja niiden peli
CREATE VIEW `Turnaukset ja niiden peli` AS
SELECT turnaukset.nimi AS `Turnaus`, pelit.Nimi AS `Peli` FROM turnaukset
JOIN pelit ON turnaukset.PeliID = pelit.PeliID;

-- Listaa kaikki kayttajat jotka ovat osallistuneet turnaukseen ja turnaukseen liittyvä peli
CREATE VIEW `Turnauksiin osallistuneet pelaajat pelin kanssa` AS
SELECT kayttajat.Nimimerkki AS `Kayttaja`, turnaukset.nimi AS `Turnaus`, pelit.Nimi AS `Peli` FROM turnaukset
JOIN pelit ON turnaukset.PeliID = pelit.PeliID
JOIN turnauksien_joukkueet ON turnaukset.TurnausID = turnauksien_joukkueet.TurnausID
JOIN joukkueet ON turnauksien_joukkueet.JoukkueID = joukkueet.JoukkueID
JOIN kayttajien_joukkueet ON joukkueet.JoukkueID = kayttajien_joukkueet.JoukkueID
JOIN kayttajat ON kayttajien_joukkueet.KayttajaID = kayttajat.KayttajaID
GROUP BY Kayttaja, Turnaus, Peli; 

-- Listaa kaikki joukkueet jotka ovat voittaneet turnauksen (Joukkueen nimi, Turnauksen nimi)
CREATE VIEW `Turnauksien voittajat` AS
SELECT t2.Nimi AS `Turnaus`, t3.Nimi AS `Joukkue` FROM turnauksien_joukkueet t1
JOIN turnaukset t2 ON t1.TurnausID = t2.TurnausID
JOIN joukkueet t3 ON t1.JoukkueID = t3.JoukkueID
WHERE sijoitus = 1;

-- Listaa kaikki kayttajat ja heidan lähettämät tiedostot
CREATE VIEW `Kayttajien tiedostot` AS
SELECT tiedostot.Nimi AS `Tiedoston nimi`, kayttajat.Nimimerkki AS `Tiedoston lahettaja` FROM kayttajien_tiedostot
JOIN tiedostot ON kayttajien_tiedostot.TiedostoID = tiedostot.TiedostoID
JOIN kayttajat ON kayttajien_tiedostot.KayttajaID = kayttajat.KayttajaID;

-- Listaa kayttajat ja heidan kommentit
CREATE VIEW `Kayttajien kommentit` AS
SELECT KommenttiID, Nimimerkki, Kommentti FROM kommentit 
JOIN kayttajat ON kommentit.KayttajaID = kayttajat.KayttajaID;

-- Listaa kayttajat ja heidan kommenttien lukumaara
CREATE VIEW `Kayttajien kommenttimaara` AS
SELECT Nimimerkki, COUNT(kommentti) AS `Kommenttien lukumaara` FROM kommentit 
JOIN kayttajat ON kommentit.KayttajaID = kayttajat.KayttajaID
GROUP BY Nimimerkki;

-- Listaa kommentit jotka liittyvät toiseen kommenttiin (kommentin kirjoittaja, kommentti, kommentti johonka kommentti liittyy)
CREATE VIEW `Kommenttien kommentit` AS
SELECT KommenttiID, Nimimerkki, Kommentti, Kommentin_kommentit_KommenttiID AS `Kommentti johon liittyy` FROM kommentit 
JOIN kayttajat ON kommentit.KayttajaID = kayttajat.KayttajaID
WHERE Kommentin_kommentit_KommenttiID IS NOT NULL;