SELECT * FROM `Joukkueiden pelaajamaarat`;

SELECT * FROM `Joukkueet ja niiden pelaajat`;
SELECT Pelaaja, Joukkue FROM `Joukkueet ja niiden pelaajat` WHERE Joukkue = 'Destination Unknown';

SELECT * FROM `Maiden pelaajamaarat`;

SELECT * FROM `Kayttajien artikkelit`;

SELECT * FROM `Turnauksiin osallistuneet joukkueet`;
SELECT * FROM `Turnauksiin osallistuneet joukkueet pelin kanssa` WHERE Joukkue = 'All-In';

SELECT * FROM `Turnauksiin osallistuneet pelaajat pelin kanssa`;
SELECT * FROM `Turnauksiin osallistuneet pelaajat pelin kanssa` WHERE Peli = 'Natural Selection';

SELECT * FROM `Turnaukset ja niiden peli`;

SELECT * FROM `Turnauksien voittajat`;

SELECT * FROM `Turnauksiin osallistuneet pelaajat pelin kanssa`;
SELECT DISTINCT Kayttaja FROM `Turnauksiin osallistuneet pelaajat pelin kanssa` WHERE Peli = 'DOTA 2';

SELECT * FROM `Kayttajien tiedostot`;

SELECT * FROM `Kommenttien kommentit`;
SELECT * FROM `Kommenttien kommentit` WHERE Nimimerkki = 'kaletenor';

SELECT * FROM `Kayttajien kommentit`;
SELECT * FROM `Kayttajien kommentit` WHERE Nimimerkki = 'abell1835';

SELECT * FROM `Kayttajien kommenttimaara`;